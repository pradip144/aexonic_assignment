1. run: npm install

2. Create .env file with the following content(change value if needed)
            PORT= 3000
            DBURI= 'mongodb://localhost:27017/test'
            JWT_SECRET = 'aexonic'
            JWT_EXPIRE_TIME='24h'

3. start mongoDB service(if in local).

4. To start the application run command:  npm start


5. To see the api documentaion open in browser: http://localhost:3000/docs