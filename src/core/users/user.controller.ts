import { Request, Response } from "express";
import { UserQueryInput, UserReturnDocument, UserUpdateInput } from "./user.model";
import { createUser, checkPassword, getUserByQuery, updateUserById, getUsersCount, formUserReturnDocument } from "./user.service";
import { generateToken, getPaginationDataFromHeader, PaginationInput, PaginationQuery, validateEmail, validateMobile } from "../../shared";

type UserSuccessResponse = {
    msg: string,
    user: UserReturnDocument,
    token?: string
}

type UsersSuccessResponse = {
    msg: string,
    users: Array<UserReturnDocument>,
    totalCount: number
}

type UserFailureResponse = {
    msg: string
}


export async function createUserHandler(req: Request, res: Response) {
     /* 	#swagger.tags = ['User']
        #swagger.description = 'create a user'
        #swagger.parameters['user'] = {
            in: 'body',
            description: 'User signup info',
            required: true,
            schema: { $ref: "#/definitions/CreateUser" }
        }
    */
    try {
        let body = req.body
        if (body && body.email && !validateEmail(body.email)) {
            throw new Error('Error!!! Invalid email.');
        }
        if (body && body.mobile && !validateMobile(body.mobile)) {
            throw new Error('Error!!! Invalid mobile number.');
        }
        const userRes = await createUser(req.body);
        if (!userRes) {
            throw new Error('Error!!! Try again later.');
        }
        const user = userRes.toJSON();
        const successRes: UserSuccessResponse = {
            msg: 'User created',
            user: formUserReturnDocument(user)
        }
        return res.send(successRes);
    } catch (error: any) {
        console.log(createUserHandler.name, error);
        const failureRes: UserFailureResponse = {
            msg: (error?.message)
        }
        return res.status(400).send(failureRes);
    }
}


export async function loginUserHandler(req: Request, res: Response) {
    /* 	#swagger.tags = ['User']
        #swagger.description = 'login user'
        #swagger.parameters['obj'] = {
            in: 'body',
            description: 'login - email and password',
            required: true,
            schema: { $ref: "#/definitions/LoginUser" }
        }
    */
    try {
        if(req.body?.email){
            const userStatus = await checkPassword(req.body.email.toLowerCase(), req.body.password);
            if (userStatus.isValid && userStatus.user) {
                const user = userStatus.user.toJSON();
                const userReturn = formUserReturnDocument(user)
                const successRes: UserSuccessResponse = {
                    msg: 'Login successful',
                    user: userReturn,
                    token: generateToken(userReturn)
                }
                return res.send(successRes);
            } else {
                throw new Error('Invalid credentials');
            }
        } else {
            throw new Error('Invalid credentials');
        }
    } catch (error: any) {
        const failureRes: UserFailureResponse = {
            msg: (error?.message)
        }
        return res.status(400).send(failureRes);
    }
}

export async function listAllUserHandler(req: Request, res: Response) {
    /* 	#swagger.tags = ['User']
        #swagger.description = 'list all users'

         #swagger.parameters['pagination'] = {
            in: 'header',
            description: 'Pagination data',
            required: false,
            schema: { $ref: "#/definitions/PaginationInputUser" }
        }
        #swagger.security = [{
            "bearerAuth": []
        }]
    */
    try {
        const userQuery: UserQueryInput = {};
        const paginationQuery: PaginationQuery = getPaginationDataFromHeader(req);
        const users = await getUserByQuery(userQuery, { pagination: paginationQuery });
        const usersReturn: UserReturnDocument[] = users.map((e) => {
            return formUserReturnDocument(e.toJSON());
        });

        const successRes: UsersSuccessResponse = {
            msg: '',
            users: usersReturn,
            totalCount: await getUsersCount(userQuery)
        }
        return res.send(successRes);

    } catch (error: any) {
        const failureRes: UserFailureResponse = {
            msg: (error?.message)
        }
        return res.status(400).send(failureRes);
    }
}


export async function updateUserHandler(req: any, res: Response) {
     /* 	#swagger.tags = ['User']
        #swagger.description = 'update user'
        #swagger.parameters['obj'] = {
            in: 'body',
            description: 'update a user',
            required: true,
            schema: { $ref: "#/definitions/UpdateUser" }
        }
        #swagger.security = [{
            "bearerAuth": []
        }]
    */
    try {
        let body = req.body
        if (body && body.mobile && !validateMobile(body.mobile)) {
            throw new Error('Error!!! Invalid mobile number.');
        }
        const updateInput: UserUpdateInput = {};
        if (req.body.firstName) {
            updateInput.firstName = req.body.firstName;
        }
        if (req.body.lastName) {
            updateInput.lastName = req.body.lastName;
        }
        const user = await updateUserById(req.user.userId, updateInput);
        if (!user) {
            throw new Error('Error!!! Try to update later.');
        }
        const userJSON = user.toJSON();
        const userReturn: UserReturnDocument = formUserReturnDocument(userJSON)
        const successRes: UserSuccessResponse = {
            msg: 'Update successful',
            user: userReturn,
        }
        return res.send(successRes);
    } catch (error: any) {
        const failureRes: UserFailureResponse = {
            msg: (error?.message)
        }
        return res.status(400).send(failureRes);
    }
}

export async function searchUserHandler(req: any, res: Response) {
    /* 	#swagger.tags = ['User']
        #swagger.description = 'search users'
        #swagger.parameters['firstName'] = {
            in: 'query',
            description: 'user first name search',
            required: false,
            schema: {type: 'string'}
        }
        #swagger.parameters['LastName'] = {
            in: 'query',
            description: 'user last name search',
            required: false,
            schema: {type: 'string'}
        }
        #swagger.parameters['email'] = {
            in: 'query',
            description: 'user email search',
            required: false,
            schema: {type: 'string'}
        }
        #swagger.parameters['mobile'] = {
            in: 'query',
            description: 'user mobile number search',
            required: false,
            schema: {type: 'string'}
        }

         #swagger.parameters['pagination'] = {
            in: 'header',
            description: 'Pagination data',
            required: false,
            schema: { $ref: "#/definitions/PaginationInputUser" }
        }

        #swagger.security = [{
            "bearerAuth": []
        }]
    */

    try {
        const filterKeys = ['email', 'firstName', 'lastName', 'mobile'];
        const filterQuery = req.query;
        let userQuery: any = {};
        filterKeys.forEach((filterKey) => {
            if (filterKey in filterQuery)
                userQuery[filterKey] = new RegExp(filterQuery[filterKey], 'i');
        })

        if (!Object.keys(userQuery).length) {
            throw new Error('Please add valid filter');
        }
        const paginationQuery: PaginationQuery = getPaginationDataFromHeader(req);
        const users = await getUserByQuery(userQuery, { pagination: paginationQuery });
        const usersReturn: UserReturnDocument[] = users.map((e) => {
            return formUserReturnDocument(e.toJSON());
        });
        const successRes: UsersSuccessResponse = {
            msg: '',
            users: usersReturn,
            totalCount: await getUsersCount(userQuery)
        }
        return res.send(successRes);
    } catch (error: any) {
        const failureRes: UserFailureResponse = {
            msg: (error?.message)
        }
        return res.status(400).send(failureRes);
    }
}