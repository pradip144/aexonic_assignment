
import mongoose from "mongoose";
import bcrypt from "bcrypt";
import config from "../../shared/services/env.config";
import { nanoid } from "nanoid";

export interface UserDocument extends mongoose.Document {
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    mobile: string;
    address: string;
    userId: string;
    createdAt: Date;
    updatedAt: Date;
    comparePassword(candidatePassword: string): Promise<boolean>;
}

export type UserReturnDocument = {
    email: string,
    firstName: string,
    lastName: string,
    mobile: string,
    address: string,
    userId: string
}

export type UserQueryInput = {
    email?: string,
    userId?: string
    firstName?: string,
    lastName?: string,
    mobile?: string
}

export type UserUpdateInput = {
    firstName?: string,
    lastName?: string,
    address?: string,
    mobile?: string
}

export type UserCreateInput = {
    firstName: string,
    lastName: string,
    email: string,
    mobile: string,
    address: string,
    password: string
}

const UserSchema = new mongoose.Schema(
    {
        _id: { type: String },
        email: { type: String, required: true, unique: true },
        firstName: { type: String, required: true },
        lastName: { type: String, required: true },
        mobile: { type: String, required: true},
        address: {type: String, required: true},
        password: { type: String, required: true },
        userId: { type: String, unique: true }
    },
    { timestamps: true, _id: false }
);

UserSchema.pre("save", async function (next) {
    let user: UserDocument = this as UserDocument;

    // if required change email to lowercase before saving

    // hashing password when password is created/modified
    if (!user.isModified("password")) return next();

    // hashing
    const rounds: number = config.has("SALT_WORK_FACTOR") ? Number(config.get("SALT_WORK_FACTOR")) : 10;
    const salt = await bcrypt.genSalt(rounds);
    const hash = await bcrypt.hashSync(user.password, salt);

    // replacing plain text password
    user.password = hash;

    // adding unique documentId
    if (!user._id) {
        user.email = user.email.toLowerCase();
        user.userId = nanoid();
        user._id = user.userId;
    }
 

    return next();
});

// password verification method
UserSchema.methods.comparePassword = async function (
    candidatePassword: string
) {
    const user: UserDocument = this;

    return bcrypt.compare(candidatePassword, user.password).catch((e) => false);
};

const User = mongoose.model<UserDocument>("user", UserSchema);

export default User;