
import express, { Request } from "express";
const router = express.Router();
import { createUserHandler, loginUserHandler, updateUserHandler, listAllUserHandler, searchUserHandler } from "./user.controller";
import { authenticateToken } from "../../shared";

router.post('/', createUserHandler);

router.post('/login', loginUserHandler);

router.get('/list', authenticateToken, listAllUserHandler);

router.put('/', authenticateToken, updateUserHandler);

router.get('/search', authenticateToken, searchUserHandler);

module.exports = router;