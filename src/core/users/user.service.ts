import { DocumentDefinition, FilterQuery, FlattenMaps, LeanDocument } from "mongoose";
import { PaginationInput } from "../../shared";
import { parseError } from "../../shared/services/mongo.service";
import User, { UserCreateInput, UserDocument, UserQueryInput, UserReturnDocument, UserUpdateInput } from "./user.model";
 
export async function createUser(user: UserCreateInput) {
    try {
        user.email = user.email!.toLowerCase();
        const userToCreate: UserCreateInput & {createdBy?: string, updatedBy?: string} = {
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
            password: user.password,
            address: user.address,
            mobile: user.mobile
        };
        return await User.create(userToCreate);
    } catch (error) {
        console.log(parseError(error));
        
        throw new Error(parseError(error).message);
    }
}

export async function updateUserById(id: string, updateInput: UserUpdateInput) {
    try {
        return await User.findOneAndUpdate({ userId: id }, updateInput, { new: true });
    } catch (error) {
        console.log(error);
        throw new Error(parseError(error).message);
    }
}

export async function getUserByQuery(query: UserQueryInput,  options: {pagination?: PaginationInput } = { pagination: {} }) {
    try {
        return await User.find(query, null, options.pagination);
    } catch (error) {
        console.log(error);
        throw new Error(parseError(error).message);
    }
}

export async function checkPassword(email: string, password: string) {
    try {
        const user = await User.findOne({email});
        const isValid = user ? await user.comparePassword(password) : false;
        if (isValid) return { isValid, user }
        return { isValid }
    } catch(error) {
        console.log(error);
        throw new Error(parseError(error).message);
    }
}

export async function getUsersCount(query: UserQueryInput) {
    try {
        return await User.countDocuments(query);
    } catch (error) {
        throw new Error(parseError(error).message)
    }
}

export function formUserReturnDocument( user: FlattenMaps<LeanDocument<UserDocument & {_id: any}>>) {
    const userReturnDoc: any = {
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        userId: user.userId,
        mobile: user.mobile,
        address: user.address
    }

    const optionalSimpleKeys: Array<keyof UserReturnDocument> = [];

    optionalSimpleKeys.forEach((key) => {
        if (key in user) {
            userReturnDoc[key] = user[key]
        }
    });

    return userReturnDoc as UserReturnDocument
}