import mongoose, { connection, ConnectOptions } from 'mongoose';
import config from "./env.config";

function connect() {
    console.log(config.get("DBURI"))
    const dbUri: string = config.get("DBURI");

    const connectOpt: ConnectOptions = {
    };

    return mongoose.connect(dbUri).then(() => {
        console.log('DB connected');
    }).catch(err => {
        console.error("DB connect error", err);
    })
}

export function parseError(err: any) {
    var data: {
        name: string,
        message: string,
        code: number
    } = {
        name: err.name,
        message: err.message.replace(new RegExp('"', 'g'), ''),
        code: err.code
    };

    if (data.message.includes('validation failed:')) {
        data.message = data.message.match(/`([\`\w\s]*)/gi)?.join(', ') || ''
    }
    switch (err.code) {
        case 11000:
            // data.index = err.message.split('$', 2)[1].split(' ', 2)[0];
            let dupKeyObj: string =  err.message.split("index: ")[1].split("_1")[0];
            data.message = `duplicate key error: ${dupKeyObj}`;
            break;
    }

    return data;
};

export default connect;