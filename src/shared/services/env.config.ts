import dotenv from "dotenv"

dotenv.config();
const config: {
    [key: string]: string | undefined
} =  {
    PORT: process.env.PORT,
    // HOST: process.env.HOST,
    DBURI: process.env.DBURI,
    JWT_SECRET: process.env.JWT_SECRET,
    JWT_EXPIRE_TIME: process.env.JWT_EXPIRE_TIME
}

export default {
    get: (key: string) => {
        return config[key] || '';
    },
    has: (key: string) => {
        return config[key] ? true : false;
    }
}

