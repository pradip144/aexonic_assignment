import { NextFunction, Request, Response } from "express";
import jwt from "jsonwebtoken";
import config from "./services/env.config";
import { UserDocument, UserReturnDocument } from "../core/users/user.model";

const jwtOptions = {
    secret: config.get("JWT_SECRET") as string,
    expiresIn: config.get("JWT_EXPIRE_TIME") as string,
}

export function authenticateToken(req: any, res: Response, next: NextFunction) {
    const authHeader = req.headers['authorization']
    const authToken = authHeader && authHeader.split(' ')[1]

    if (authToken == null) return res.sendStatus(401)

    jwt.verify(authToken, jwtOptions.secret, (err: any, user: any) => {

        if (err) { console.log(err); return res.status(403).send({ msg: (err) }) }

        // verify email from token with req.user
        req.user = user;
        next()
    })
}

export function generateToken(user: UserReturnDocument) {
    return jwt.sign(user, jwtOptions.secret, { expiresIn: jwtOptions.expiresIn });
}

export function getPaginationDataFromHeader(req: any) {
    const paginationReq: PaginationInput = req.headers['pagination'] ? JSON.parse(req.headers['pagination']) : {};
    if (!paginationReq.limit) paginationReq.limit = 2;
    if (!paginationReq.page) paginationReq.page = 1;
    const paginationQuery: PaginationQuery = {
        sort: {},
        limit: paginationReq.limit,
        skip: (paginationReq.page - 1) * paginationReq.limit
    }
    if (paginationReq.sortBy) {
        paginationQuery.sort = {
            [paginationReq.sortBy]: paginationReq.sortOrder || 1
        }
    }
    return paginationQuery;
}

const regexObj = {
    email: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
    mobile: /^([0|\+[0-9]{1,5})?([0-9]{10})$/
}

export function validateEmail(email: string) {
    return (email.match(regexObj.email)?.[0] === email);
}

export function validateMobile(mobile: string) {
    return (mobile.match(regexObj.mobile)?.[0] === mobile);
}

export type PaginationInput = {
    sortBy?: string,
    sortOrder?: 1 | -1,
    limit?: number,
    page?: number
}

export type PaginationQuery = {
    sort: { [key: string]: 1 | -1 } | {},
    limit: number,
    skip: number
}