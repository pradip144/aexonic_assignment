import express, { Request, Response } from "express";
const router = express.Router();
const userRouter = require('../core/users/user.router');

router.get('/', (req: Request, res: Response) => {
    res.redirect("/health");
})

// check health
router.get("/health", (req: Request, res: Response) => {
    res.status(200).send('Active');
})

router.use('/users', userRouter);


router.all('*', (req: Request, res: Response) => {
    res.status(404).send('Wrong address');
})

module.exports = router;