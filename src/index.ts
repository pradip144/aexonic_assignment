import express from "express";
import envConfig from "./shared/services/env.config";
import connect from "./shared/services/mongo.service";
import swaggerUi from "swagger-ui-express";
const routes = require('./router');

const port: number = Number(envConfig.get("PORT"));
// const host: string = envConfig.get("HOST");

const app = express();


// swagger setup

const swaggerDoc = require('./swagger.json');
const swaggerOpt = {
  explorer: false
}
app.use(`/docs`, swaggerUi.serve, swaggerUi.setup(swaggerDoc, swaggerOpt));

// json request parser
app.use(express.json());
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.use(express.urlencoded({ extended: false }));

// add routes
app.use('/', routes);

// start server
app.listen(port, () => {
  console.log(`Server started at ${port}`);
  connect();
});